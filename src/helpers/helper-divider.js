module.exports.divider = function (params) {
	return '\
		<tr class="divider">\
		  <td class="one-column" valign="top" height="1">\
		    <table width="'+params.hash.width+'" height="1" style="width:'+params.hash.width+';">\
		      <tr>\
		        <td class="inner contents" bgcolor="'+ params.hash.bgcolor +'" height="1" style="line-height:1px;"></td>\
		      </tr>\
		    </table>\
		  </td>\
		</tr>\
	'
};